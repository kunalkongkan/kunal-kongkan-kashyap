import React from 'react'

const Marquee = () => {
    return (
        <div className="marquee relative overflow-hidden mb-4">
            <div className="marquee__inner w-fit flex relative" aria-hidden="true">
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold stroke_text">PROJECTS</h2>
                <h2 className="text-6xl lg:text-8xl font-teko font-semibold">PROJECTS</h2>
            </div>
        </div>
    )
}

export default Marquee